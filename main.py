from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import sys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import vlc
import pafy


url = "https://meet.jit.si/" + sys.argv[1]
media_player = vlc.MediaPlayer()
queue = []
options = Options()
options.headless = False
options.add_argument("--mute-audio")
options.set_preference("permissions.default.microphone", 1)
options.set_preference("media.volume_scale", "0.0")
options.set_preference("permissions.default.camera", 1)
driver = webdriver.Firefox(options=options)
driver.get(url)
print("Connecting to: " + driver.title)
inp = driver.find_elements(By.TAG_NAME,"input")
print(inp)
inp[0].send_keys("Patrick" + Keys.RETURN)
driver.implicitly_wait(10)
chat = driver.find_elements(By.XPATH, "//div[@aria-label='Open / Close chat']")
if len(chat) > 0:
    print("chat working")
    #assert driver.find_elements(By.CLASS_NAME, "overlay__content") == []
    c= chat[0]
    c.click()
    driver.implicitly_wait(10)
    own = driver.find_elements(By.ID, "usermsg")
    own[0].send_keys("ok" + Keys.RETURN)
    msg = driver.find_elements(By.CLASS_NAME,"usermessage")
    print("Ready")
    print(len(msg))
    last_msg = len(msg)-1 
    while True:
        msg = driver.find_elements(By.CLASS_NAME,"usermessage")
        if (len(msg)-1) > last_msg:
            cur_msg = msg[len(msg)-1].text
            last_msg = len(msg)-1
            print(cur_msg)
            cur_msg = cur_msg[cur_msg.index('\n')+1:]
            print(cur_msg)
            if cur_msg[0] == '!':
                cmd = cur_msg[1:]
                cmd = cmd.split()
                print("Command: " + '"'+ cmd[0]+ '"')
                if "help" in cmd[0]:
                    print("Here is help")
                elif cmd[0] == "play" and len(cmd)>1:
                    print("Playing: " + cmd[1])
                    queue.append(cmd[1])
                    print(queue)
        if not media_player.is_playing():
            try:
                print(queue)
                video = pafy.new(queue[0])
                audio = video.getbestaudio()
                media = media_player.set_media(vlc.Media(audio.url))
                media_player.play()
                queue.remove(queue[0])
            except:
                p = 0

if input("Enter close\n") == "close":   
    driver.close()
